/*
  Copyright (c) 2018-2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.sampler.lifecycle

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.commonsware.jetpack.sampler.lifecycle.databinding.RowBinding

internal class EventAdapter(
  private val inflater: LayoutInflater,
  private val startTime: Long
) : ListAdapter<Event, EventViewHolder>(EventDiffer) {

  override fun onCreateViewHolder(
    parent: ViewGroup,
    viewType: Int
  ) = EventViewHolder(RowBinding.inflate(inflater, parent, false), startTime)

  override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
    holder.bindTo(getItem(position))
  }

  private object EventDiffer : DiffUtil.ItemCallback<Event>() {
    override fun areItemsTheSame(oldEvent: Event, newEvent: Event) =
      oldEvent === newEvent

    override fun areContentsTheSame(oldEvent: Event, newEvent: Event) =
      oldEvent == newEvent
  }
}