/*
  Copyright (c) 2019-2021 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.sampler.fragments

import android.os.Bundle
import android.text.format.DateUtils
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import com.commonsware.jetpack.sampler.fragments.databinding.TodoDisplayBinding
import com.commonsware.jetpack.sampler.util.ViewBindingFragment

private const val ARG_MODEL_ID = "modelId"

class DisplayFragment :
  ViewBindingFragment<TodoDisplayBinding>(TodoDisplayBinding::inflate) {
  companion object {
    fun newInstance(modelId: String) = DisplayFragment().apply {
      arguments = bundleOf(ARG_MODEL_ID to modelId)
    }
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    val vm: DisplayViewModel by viewModels()
    val model = vm.getModel(
      arguments?.getString(ARG_MODEL_ID)
        ?: throw IllegalStateException("no modelId provided!")
    )

    model?.let {
      useBinding { binding ->
        binding.model = model

        binding.createdOnFormatted = DateUtils.getRelativeDateTimeString(
          activity,
          model.createdOn.toEpochMilli(), DateUtils.MINUTE_IN_MILLIS,
          DateUtils.WEEK_IN_MILLIS, 0
        )
      }
    }
  }
}