/*
  Copyright (c) 2017-2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.work.download

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.work.Constraints
import androidx.work.Data
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkInfo
import androidx.work.WorkManager

class DownloadViewModel(application: Application) :
  AndroidViewModel(application) {
  val liveWorkStatus = MediatorLiveData<WorkInfo>()

  fun doTheDownload() {
    val downloadWork = OneTimeWorkRequest.Builder(DownloadWorker::class.java)
      .setConstraints(
        Constraints.Builder()
          .setRequiredNetworkType(NetworkType.CONNECTED)
          .setRequiresBatteryNotLow(true)
          .build()
      )
      .setInputData(
        Data.Builder()
          .putString(
            DownloadWorker.KEY_URL,
            "https://commonsware.com/Android/source_1_0.zip"
          )
          .build()
      )
      .addTag("download")
      .build()
    val unZIPWork = OneTimeWorkRequest.Builder(UnZIPWorker::class.java)
      .setConstraints(
        Constraints.Builder()
          .setRequiresStorageNotLow(true)
          .setRequiresBatteryNotLow(true)
          .build()
      )
      .setInputData(
        Data.Builder()
          .putString(DownloadWorker.KEY_RESULTDIR, "unzipped")
          .build()
      )
      .addTag("unZIP")
      .build()

    WorkManager.getInstance(getApplication())
      .beginWith(downloadWork)
      .then(unZIPWork)
      .enqueue()

    val liveOpStatus = WorkManager.getInstance(getApplication())
      .getWorkInfoByIdLiveData(unZIPWork.id)

    liveWorkStatus.addSource(liveOpStatus) { workStatus ->
      liveWorkStatus.value = workStatus

      if (workStatus.state.isFinished) {
        liveWorkStatus.removeSource(liveOpStatus)
      }
    }
  }
}
