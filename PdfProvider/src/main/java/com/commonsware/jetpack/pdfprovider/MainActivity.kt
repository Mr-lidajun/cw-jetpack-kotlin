/*
  Copyright (c) 2019-2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.pdfprovider

import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider

import com.commonsware.jetpack.pdfprovider.databinding.ActivityMainBinding

private const val AUTHORITY = "${BuildConfig.APPLICATION_ID}.provider"

class MainActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    val binding = ActivityMainBinding.inflate(layoutInflater)

    setContentView(binding.root)

    val motor: MainMotor by viewModels()

    motor.states.observe(this) { state ->
      when (state) {
        MainViewState.Loading -> {
          binding.export.isEnabled = false
          binding.view.isEnabled = false
        }
        is MainViewState.Content -> {
          binding.export.isEnabled = false
          binding.view.isEnabled = true
          binding.view.setOnClickListener {
            val uri = FileProvider.getUriForFile(this, AUTHORITY, state.pdf)
            val intent = Intent(Intent.ACTION_VIEW)
              .setDataAndType(uri, "application/pdf")
              .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

            try {
              startActivity(intent);
            } catch (ex: ActivityNotFoundException) {
              Toast.makeText(
                this,
                "Sorry, we cannot display that PDF!",
                Toast.LENGTH_LONG
              ).show()
            }
          }
        }
        is MainViewState.Error -> {
          binding.export.isEnabled = false
          binding.view.isEnabled = false
          binding.error.text = state.throwable.localizedMessage
        }
      }
    }

    binding.export.setOnClickListener { motor.exportPdf() }
  }
}
