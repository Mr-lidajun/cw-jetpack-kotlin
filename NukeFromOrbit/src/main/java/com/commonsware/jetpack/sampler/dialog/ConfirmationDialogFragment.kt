/*
  Copyright (c) 2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.sampler.dialog

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment

class ConfirmationDialogFragment : DialogFragment() {
  override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
    val navController = NavHostFragment.findNavController(this)
    val viewModelProvider =
      ViewModelProvider(navController.getViewModelStoreOwner(R.id.nav_graph))
    val vm = viewModelProvider.get(GraphViewModel::class.java)

    return AlertDialog.Builder(requireActivity())
      .setTitle(R.string.dialog_title)
      .setMessage(R.string.dialog_message)
      .setPositiveButton(R.string.dialog_positive) { _, _ -> vm.onAccept() }
      .setNegativeButton(R.string.dialog_negative) { _, _ -> vm.onDecline() }
      .setOnCancelListener { vm.onDecline() }
      .create()
  }
}