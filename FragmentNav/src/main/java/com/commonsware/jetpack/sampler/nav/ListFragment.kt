/*
  Copyright (c) 2019-2021 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.sampler.nav

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.commonsware.jetpack.sampler.nav.databinding.TodoRosterBinding
import com.commonsware.jetpack.sampler.util.ViewBindingFragment

class ListFragment :
  ViewBindingFragment<TodoRosterBinding>(TodoRosterBinding::inflate) {
  private val vm: ListViewModel by viewModels()

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    useBinding { binding ->
      binding.items.layoutManager = LinearLayoutManager(context)

      val adapter = ToDoListAdapter(layoutInflater) {
        navTo(it)
      }

      binding.items.adapter = adapter.apply { submitList(vm.items) }
    }
  }

  private fun navTo(model: ToDoModel) {
    findNavController().navigate(ListFragmentDirections.displayModel(model.id))
  }
}